//
//  ViewController.swift
//  JSCLounge_test_JOD_ArdyStefanus
//
//  Created by Ardy Stefanus Sunarno on 26/01/21.
//

import UIKit
import Foundation

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableViewVC: UITableView!
    
    let listKontak = [kontakManager]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        
        tableViewVC.delegate = self
        tableViewVC.dataSource = self
        
        let urlString = "https://randomuser.me/api?results=5&exc=login,registered,id,nat&nat=us&noinfo"
        
        let url = URL(string: urlString)
        
        guard url != nil else {
            return
        }
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: url!) { data, response, error in
            
            if error == nil && data != nil {
                
                let decoder = JSONDecoder()
                
                do
                {
                    let kontakManage = try decoder.decode(kontakManager.self, from: data!)
                    print(kontakManage)
                }
                
                catch
                {
                    print("Error parse JSON")
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listKontak.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        
        cell.textLabel?.text = listKontak[indexPath.row].gender
        
        return cell
    }

    func setNavigationBar() {
        let screenSize: CGRect = UIScreen.main.bounds
        let navBar = UINavigationBar(frame: CGRect(x: 0, y: 40, width: screenSize.width, height: 50))
        let navItem = UINavigationItem(title: "Kelola Kontak")
        let doneItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: #selector(done))
        navItem.rightBarButtonItem = doneItem
        navBar.setItems([navItem], animated: false)
        self.view.addSubview(navBar)
    }
    
    @objc func done() { // remove @objc for Swift 3
        print("tes")
    }
}

