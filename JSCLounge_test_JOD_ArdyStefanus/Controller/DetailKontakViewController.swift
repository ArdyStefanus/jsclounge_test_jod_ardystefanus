//
//  DetailKontakViewController.swift
//  JSCLounge_test_JOD_ArdyStefanus
//
//  Created by Ardy Stefanus Sunarno on 26/01/21.
//

import UIKit

class DetailKontakViewController: UIViewController {

    @IBOutlet weak var imageDetailKontak: UIImageView!
    @IBOutlet weak var namaKontak: UILabel!
    @IBOutlet weak var isiLahir: UILabel!
    @IBOutlet weak var isiKelamin: UILabel!
    @IBOutlet weak var isiEmail: UILabel!
    @IBOutlet weak var isiTelepon: UILabel!
    @IBOutlet weak var isiSelular: UILabel!
    @IBOutlet weak var isiTempatTinggal: UILabel!
    @IBOutlet weak var isiKoordinat: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeButton(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
