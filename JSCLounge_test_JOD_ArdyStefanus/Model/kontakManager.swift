//
//  kontakManager.swift
//  JSCLounge_test_JOD_ArdyStefanus
//
//  Created by Ardy Stefanus Sunarno on 26/01/21.
//

import Foundation

struct kontakManager: Codable
{
    let name: [namaKontak]
    let gender: String
    let email: String
    let telpon: String
    let selular: String
    let locStreet: [street]
    let locCoor: [coordinat]
}

struct namaKontak: Codable {
    let title: String
    let first: String
    let last: String
}

struct street: Codable {
    let number: Int
    let nameStreet: String
}

struct coordinat: Codable {
    let lat: String
    let long: String
}
